#!/usr/bin/env python
##################################################
#
# SMS & Email Alert python interface
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# March 2017
#
##################################################

import smtplib

class alert:
    def __init__(self,fromUser,fromPwd):
        self.verbose=False
        self.fromUser = str(fromUser)
        self.fromPwd = str(fromPwd)
        self.smtpserver = smtplib.SMTP("smtp.cern.ch",587)
        self.smtpserver.ehlo()
        self.smtpserver.starttls()
        self.smtpserver.ehlo
        self.smtpserver.login(fromUser, fromPwd)
        pass
    def setVerbose(self,enable):
        self.verbose=enable
        pass
    def send(self,to,subject,message):
        header = 'To:' + to + '\n' + 'From: ' + self.fromUser + '\n' + 'Subject:%s'%subject+'\n'	
        msg = header+"\n"+message
        ret = self.smtpserver.sendmail(self.fromUser, to, msg)
        if self.verbose:
            print header
	    print 'done!'
        return ret
    def close(self):
        self.smtpserver.close()
        pass
    pass

if __name__=="__main__":
    import argparse
    import base64
    sms = alert('adecmos@cern.ch',base64.b64decode("S2lnZUJlbGUxNw=="))
    print sms.send("ep-ade-id-tfm@cern.ch","Test","This is a test")
    sms.close()
    pass
