#!/usr/bin/env python
#############################################
# X-ray Irradiation of TowerJazz Investigator I 
#
# Abhishek.Sharma@cern.ch
# Carlos.Solans@cern.ch
# January 2018
#############################################

from SerialCom import SerialCom
import Keithley
import time
import signal
import smtplib
import base64
import elog
import alert
import argparse

'''
parser=argparse.ArgumentParser()
parser.add_argument("voltage",type=float,nargs=3,help="[Vmin,Vmax,steps]")
parser.add_argument("output")
parser.add_argument("-c","--center",help="Center X Y Z (mm)",type=float,nargs=3,default=[56.0912,84.2742,4.7])
parser.add_argument("-X","--X_Len",help="Range in X (mm)",type=float,default=10)
parser.add_argument("-Z","--Z_Len",help="Range in Z (mm)",type=float,default=0)
parser.add_argument("-x","--x_steps",help="Number of steps in X",type=int,default=20)
parser.add_argument("-y","--y_steps",help="Number of steps in Y",type=int,default=20)
parser.add_argument("-z","--z_steps",help="Number of steps in Z",type=int,default=1)
parser.add_argument("-n","--n_daqs",help="Number of events per setting",type=int,default=100)
parser.add_argument("-d","--debug",help="Enable debug mode",action='store_true')
parser.add_argument("-a","--ampliV",help="Voltage for amplifier",type=float,default=12.)
parser.add_argument("-I","--I_lim",help="Current limit (uA)", type=float,default=6)
parser.add_argument("-D","--delay",help="Trigger delay (120 ns)",type=int,default=120)
parser.add_argument("-F","--frequency",help="Sampling frequency (5 GHz)",type=float,default=5)
parser.add_argument("-T","--trigger",help="Trigger channel",type=int,default=0)
parser.add_argument("-P","--polarity",help="Rising or faling edge",type=bool,default=False)
parser.add_argument("-L","--level",help="Voltage level (V)",type=float,default=-0.5)
parser.add_argument("-b","--batch",help="Enable batch mode",default=False,action='store_true')
parser.add_argument('-Abhi','--Abhi',action='store_true',help="Alert Abhishek Only")
parser.add_argument('-A','--author',help="Author name for elog run entry",type=str,required=True)
parser.add_argument('-S','--subject',help="Elog subject",type=str,default="")
parser.add_argument('-C','--comments',help="Elog comments on run",type=str,default="")
args=parser.parse_args()
'''

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

signal.signal(signal.SIGINT, signal_handler)

'''
KeithIFOL1  = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXA")
KeithIFOL2  = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXB")
KeithIFOL3  = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXC")
KeithIFOL4  = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXD")
KeithPWR    = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXE")
KeithHV     = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXF")
KeithStatus = Keithley.Keithley("/dev/tty.usbserial-FTZ5U8CXG")

PSupplies = [KeithIFOL1,KeithIFOL2,KeithIFOL3,KeithIFOL4,KeithPWR,KeithHV]

currLim = 0.00001
voltLim = 2.5

if currLim > 0.00001: cont = False
if voltLim > 2.5:     cont = False

for PS in PSupplies:
    PS.setCurrentLimit(currLim)
    Ps.setVoltageLimit(voltLim)
    PS.enableOutput(True)
    PS.setVoltage(0)

keithPWR.setVoltage(1.8)
keithHV.setVoltage(-1)

if (keithIFOL1.getCurrent() |
    keithIFOL2.getCurrent() |
    keithIFOL3.getCurrent() |
    keithIFOL4.getCurrent() |
    keithPWR.getCurrent()   |
    keithHV.getCurrent())   > 0.00001: cont = False
'''

fw = open("%s_XrayCurrentData.txt"%time.strftime("%Y%m%d_%Hh%Mm%Ss"),"w+")

fw.write("Time(s),IFOL1(uA),IFOL2(uA),IFOL3(uA),IFOL4(uA),IPWR(uA),IHV(uA)\n")

status = 1
while cont:
    IFOL1 = 1#keithIFOL1.getCurrent()*(10**6)
    IFOL2 = 2#keithIFOL2.getCurrent()*(10**6)
    IFOL3 = 3#keithIFOL3.getCurrent()*(10**6)
    IFOL4 = 4#keithIFOL4.getCurrent()*(10**6)
    IPWR  = 5#keithPWR.getCurrent()*(10**6)
    IHV   = 6#keithHV.getCurrent()*(10**6)
    print "IFOL1: %0.3f\tIFOL2: %0.3f\tIFOL3: %0.3f\tIFOL4: %0.3f\tIPWR: %0.3f\tIHV: %0.3f\t\n"%(IFOL1,IFOL2,IFOL3,IFOL4,IPWR,IHV)
    status  = 2#keithStatus.getVoltage()
    if status != 2:
        cont=False
        status = 0
        break
    #if (IFOL1 | IFOL2 | IFOL3 | IFOL4 | IPWR | IHV) > 10: SafetyCheck = False
    fw.write("%s,%s,%s,%s,%s,%s,%s\n"%(time.time(),IFOL1,IFOL2,IFOL3,IFOL4,IPWR,IHV))
    time.sleep(1)

fw.close()

'''
for PS in PSupplies:
    PS.setVoltage(0)
    PS.enableOutput(False)
    PS.close()
'''

if status == 0: log = "X-ray Machine Stalled!"
else:          log = "Xray Measurement Complete."
    
entry = elog.elog("ade-pixel-elog",8080)#IDdown	Date	Author	Type	Subject	Sample	Matrix	Target Dose	
#if cont: entry.submit("Laser",{"Author":"Abhishek Sharma","Subject":"TowerJazz Investigator I Irradiation","Category":"Measurement","Text":log}	)
entry.submit("Laser",{"Author":"test","Subject":"test","Category":"Measurement","Text":log}	)
#else: to = 'ep-ade-id-tfm@cern.ch'
user = 'adecmos@cern.ch'
pwd = str(base64.b64decode("S2lnZUJlbGUxNw=="))
sms = alert.alert(user,pwd)
subject = "X-ray Irradiation"
#sms.send(to,subject,log)



















'''
#Set all current limits
keithIFOL1.setCurrentLimit(currLim)
keithIFOL2.setCurrentLimit(currLim)
keithIFOL3.setCurrentLimit(currLim)
keithIFOL4.setCurrentLimit(currLim)
keithPWR.setCurrentLimit(currLim)
keithHV.setCurrentLimit(currLim)

#Set all voltage limits
keithIFOL1.setVoltageLimit(voltLim)
keithIFOL2.setVoltageLimit(voltLim)
keithIFOL3.setVoltageLimit(voltLim)
keithIFOL4.setVoltageLimit(voltLim)
keithPWR.setVoltageLimit(voltLim)
keithHV.setVoltageLimit(voltLim)

#Switch all power supplies on
keithIFOL1.enableOutput(True)
keithIFOL2.enableOutput(True)
keithIFOL3.enableOutput(True)
keithIFOL4.enableOutput(True)
keithPWR.enableOutput(True)
keithHV.enableOutput(True)

#Set all voltages
keithIFOL1.setVoltage(0)
keithIFOL2.setVoltage(0)
keithIFOL3.setVoltage(0)
keithIFOL4.setVoltage(0)
keithPWR.setVoltage(1.8)
keithHV.setVoltage(-1)

#Switch all power supplies off
keithIFOL1.enableOutput(False)
keithIFOL2.enableOutput(False)
keithIFOL3.enableOutput(False)
keithIFOL4.enableOutput(False)
keithPWR.enableOutput(False)
keithHV.enableOutput(False)

keithIFOL1.close()
keithIFOL2.close()
keithIFOL3.close()
keithIFOL4.close()
keithPWR.close()
keithHV.close()

'''
