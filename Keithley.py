#!/usr/bin/env python
#############################################
# Keithley 2400 
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from time import sleep
from SerialCom import SerialCom

class Keithley:
    sc = None
    def __init__(self,portname,_baudrate=19200):
        self.sc = SerialCom(portname,timeout=1)
    def setCurrentLimit(self,fvalAmps):
        self.sc.write(":SENSE:CURR:PROT %f"%fvalAmps)
    def setVoltageLimit(self,fvalVolts):
        self.sc.write(":SENSE:VOLT:PROT %f"%fvalVolts)
    def setCurrent(self,fsetValAmps):
        self.sc.write(":SOUR:CURR %f"%fsetValAmps)
    def setVoltage(self,fsetValVolts):
        self.sc.write(":SOUR:VOLT %f"%fsetValVolts)
    def enableOutput(self, bEnable):
        if bEnable == True:
            self.sc.write(":OUTPUT ON")
        elif bEnable == False:
            self.sc.write(":OUTPUT OFF")
    def close(self):
        self.sc.close()
    def getVoltage(self):
        v = self.read()
        if not "," in v: return -999.
        return float(v.split(",")[0])
    def measureVoltage(self,voltLim,voltRange):
        #auto-range MUST NOT be used on Keithley during voltage measurement, will damage DUT otherwise.
        #voltage limit MUST be set higher than expected voltage, will damage DUT/Keithley otherwise.
        print "setting current source mode"
        self.sc.write(":SOUR:FUNC VOLT")
        print "setting current source mode fix"
        self.sc.write(":SOUR:CURR:MODE FIX")
        print "setting current range 0A"
        self.sc.write(":SOUR:CURR:RANG 0")
        print "setting current level 0A"
        self.sc.write(":SOUR:CURR:LEV 0")
        print "setting sense function: Volt"
        self.sc.write(":SENS:FUNC VOLT")
        print "setting voltage limit: %sV"%voltLim
        self.sc.write(":SENS:VOLT:PROT %s"%voltLim)
        print "setting voltage range: %sV"%voltRange
        self.sc.write(":SENS:VOLT:RANG %s"%voltRange)
        print "setting voltage auto-range OFF"
        self.sc.write(":SENS:VOLT:RANG:AUTO OFF")
        
        v = self.read()
        if not "," in v: return -999.
        return float(v.split(",")[0])
    def getCurrent(self):
        v = self.read()
        return float(v.split(",")[1])
    def userCmd(self,cmd):
        print "userCmd: %s" % cmd
        return self.sc.writeAndRead(cmd)
    def read(self):
        return self.sc.writeAndRead(":READ?")
    def reset(self):
        self.sc.write("*RST")
    def rampVoltage(self,fsetNewValVolts,iSteps,isetDelay):
        V = self.getVoltage()
        V = round(V,4)
        if abs(fsetNewValVolts-V)<1: 
            self.setVoltage(fsetNewValVolts)
            return
        s = 1
        iSteps = int(iSteps+1)
        Vstep = float((fsetNewValVolts-V)/(iSteps-1))
        while s < (iSteps):
            self.sc.write(":SOUR:VOLT %f"%(Vstep*s+V))
            sleep(isetDelay)
            print "Ramping Voltage: %.4f"%(Vstep*s+V)
            s += 1
            pass
